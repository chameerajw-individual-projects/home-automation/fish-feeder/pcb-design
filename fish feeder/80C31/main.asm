#include <Servo.h> 
#include <RealTimeClockDS1307.h>

#define Display_Clock_Every_N_Seconds 1   // n.secs to show date/timeint count=0;



char formatted[] = "00-00-00 00:00:00x";
int count=0;//print time
int Min,Sec,Hou=0;

int HOUR=22 , MIN=50;//feeding time

unsigned long  bl1= 0; //blinking bulb

int manFeed=0; // manual Switch
int lowFood=0;

int Buz=4;
int On=6;
int LowBulb=5;

int feedingTime, fedCount, fedTime=0; //initialize feeding, fed or not, last feed time


int servoPower=12;
int servoPin = 11; 
Servo Servo1;

void setup() {
  // digitalWrite(servoPower,HIGH);
  Serial.begin(9600);

  pinMode(servoPower,OUTPUT);
  Servo1.attach(servoPin); 
  Servo1.write(0);
 
  pinMode(On, OUTPUT); 
  pinMode(Buz, OUTPUT); 
  pinMode(LowBulb, OUTPUT);   
   
}
 
void loop() {

  RTC.readClock();
  Hou=RTC.getHours();
  Min=RTC.getMinutes();
  Sec=RTC.getSeconds();
  

  lowFoodIndicate();
  printTime();
  bulbBlink();

  digitalWrite(Buz,LOW);

  if ( abs(Hou - fedTime)>5){ //set fedCount to 0
    fedCount=0;
  }

Serial.println(millis());

  int manFeed=analogRead(A0);
  Serial.println(manFeed);
  if ( fedCount == 0 && manFeed>500){  //manually feeding
    feedingTime==1;  //stop blinking
    feeding();
  }


  
  if (Hou==HOUR && Min==MIN && 00<Sec && Sec<04 && fedCount==0){  //feed at HOUR:MIN
    feedingTime==1; //stop blinking
    feeding();
  }

  
}


void printTime(){
    count++;
    if(count % Display_Clock_Every_N_Seconds == 0){
    Serial.print(count);
    Serial.print(": ");
    RTC.getFormatted(formatted);
    Serial.print(formatted);
    Serial.println();
  }
} 


void bulbBlink(){
  if (feedingTime==0){

      while (millis() - bl1<100){
        digitalWrite(On,HIGH);  
      }

       while ((millis() - bl1)<1500){
         digitalWrite(On,LOW);  
      }
      bl1=millis();
   }
 
 }


void  feeding(){
  fedCount=1;  //set fed count
  fedTime=Hou;
  feedingTime=0;
  digitalWrite(servoPower,HIGH);
  digitalWrite(On,HIGH);
  digitalWrite(Buz,LOW);
  digitalWrite(Buz,HIGH); 
  delay(100);
  digitalWrite(Buz,LOW);
  delay(100);
  digitalWrite(Buz,HIGH); 
  delay(100);
  digitalWrite(Buz,LOW);
  delay(1000);
  

  Servo1.write(90);
  digitalWrite(Buz,HIGH);
  delay(1000);
  Servo1.write(0);
  delay(1000);

  digitalWrite(servoPower,LOW);
  digitalWrite(Buz,LOW);
  delay(1000);
  digitalWrite(Buz,HIGH); 
  delay(100);
  digitalWrite(Buz,LOW);
  delay(100);
  digitalWrite(Buz,HIGH); 
  delay(100);
  digitalWrite(Buz,LOW);
  delay(1000);
  
}



void lowFoodIndicate(){
   
   int lowFood=analogRead(A1);
   if(lowFood>500){
    digitalWrite(LowBulb,HIGH);
    if (Min==45 && 00<Sec && Sec<04){
      digitalWrite(Buz,HIGH);
      delay(1000);
      digitalWrite(Buz,LOW);
       delay(2000);
        digitalWrite(Buz,HIGH);
      delay(1000);
      digitalWrite(Buz,LOW);
    }
  }

  else {
     digitalWrite(LowBulb,LOW);
  }
 
}


